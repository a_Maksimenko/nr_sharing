novoApp.directive('novoHeader', ['getUser', 'globals', function (getUser, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'header.html',
        scope: {},
        link: function (scope) {
            getUser.data().then(function (userData) {
                scope.currentUser = userData;
            })
        }
    }
}]);