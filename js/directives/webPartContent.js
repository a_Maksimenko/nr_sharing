novoApp.directive('webPartContent', function () {
    return {
        restrict: 'A',
        scope: {
            webPartName: '@'
        },
        link: function (scope, elem, attr) {
            var $partial = jQuery('span:contains("' + scope.webPartName + '")');

            if ($partial.length) {
                var $holder = $partial.closest('table').closest('tr').next('tr').find('div:first');

                if ($holder.text() && $holder.text() !== 'Edit this page to modify your web part content.') {
                    elem.html($holder.html());
                }
            }
        }
    }
});