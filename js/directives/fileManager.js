novoApp.directive('fileManager', ['helper', 'listNames', 'dataService', 'globals', function (helper, listNames, dataService, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'fileManager.html',
        scope: {
            postId: '=',
            uploads: '='
        },
        link: function (scope, element, attrs) {
            var renderUploadTool = function () {
                jQuery('#uploadBox').SPControlUpload({
                    listName: listNames.attachments,
                    overlayBgColor: '#ff8200',
                    overlayMessage: '<span style="position: relative; top: 15px;">Please wait</span>',
                    selectFileMessage: '<span style="display: inline-block; width: 32px; height: 32px; background: url(/sites/Novorapidsharingplatform/System/images/popup-sprite.png) -20px -50px; vertical-align: middle; position: absolute; left: 0; top: 5px;"></span><span style="margin-left: 10px;">Add new file (PPT, PDF, JPG and videos)</span>',
                    onPageChange: function (ev) {
                        if (ev.state === 3 && ev.action === 'postLoad') {
                            angular.element(this).html('<strong>File successfully uploaded!</strong>');
                            ev.file.FileName = helper.removeID(ev.file.FileLeafRef);
                            scope.uploads.push(ev.file);
                            if (scope.uploads.length < 3) {
                                renderUploadTool();
                            }
                            dataService.update({
                                listName: listNames.attachments,
                                ID: ev.file.ID,
                                valuepairs: [['PostID', scope.postId]]
                            });
                        }
                    }
                });
            };

            scope.$watch('postId', function (newVal) {
                if (newVal) {
                    renderUploadTool();
                    scope.addMore = function () {
                        renderUploadTool();
                        return false;
                    };
                    scope.remove = function (upload) {
                        var confirmed = window.confirm('Are you sure?');

                        if (!confirmed) return false;
                        dataService.remove({
                            listName: listNames.attachments,
                            FileRef: '/' + helper.removeID(upload.FileRef)
                        }).then(function () {
                            angular.forEach(scope.uploads, function (item, index) {
                                if (item.ID === upload.ID) {
                                    (scope.uploads).splice(index, 1);
                                }
                            });
                            if (scope.uploads.length === 2) {
                                renderUploadTool();
                            }
                        });
                        return false;
                    };
                }
            });
        }
    }
}]);