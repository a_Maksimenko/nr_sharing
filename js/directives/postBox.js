novoApp.directive('postBox', ['dataService', 'listNames', 'helper', 'likesManager', 'postViewStatistic', 'globals', '$sce', function (dataService, listNames, helper, likesManager, postViewStatistic, globals, $sce) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'postBox.html',
        scope: {},
        link: function (scope, elem, attr) {
            scope.currentPostID = helper.getUrlVars().postID;
            if (scope.currentPostID) {
                scope.likePost = function (ID) {
                    likesManager.updateLikesNumber('post', ID, scope.post);
                };

                var listQuery = {
                        "post": "<Query><Where><Eq><FieldRef Name='ID'></FieldRef><Value Type='Number'>" + scope.currentPostID + "</Value></Eq></Where></Query>",
                        "attachments": "<Query><Where><Eq><FieldRef Name='PostID'></FieldRef><Value Type='Text'>" + scope.currentPostID + "</Value></Eq></Where></Query>"
                    },
                    getPost = function () {
                        return dataService.read({
                            listName: listNames.posts,
                            CAMLQuery: listQuery.post
                        })
                    },
                    getAttachments = dataService.read({
                        listName: listNames.attachments,
                        CAMLQuery: listQuery.attachments
                    });

                getPost().then(function (res) {
                    //post view statistics update
                    if (postViewStatistic.urlChanged) {
                        var viewCounter = res[0]['Counter_x003a_Views'] ? parseInt(res[0]['Counter_x003a_Views']) : 0;

                        dataService.update({
                            listName: listNames.posts,
                            ID: scope.currentPostID,
                            valuepairs: [['Counter_x003a_Views', viewCounter + 1]]
                        });
                        res[0]['Counter_x003a_Views'] = viewCounter + 1;
                    }

                    res[0].BodyText = $sce.trustAsHtml(res[0].BodyText);
                    res[0].Author = helper.parseName(res[0].Author)["initials"].split(";#")[1];
                    res[0].SubmissionDate = helper.getRelativeTime(res[0].Created);
                    res[0].LikesAmount = res[0].UserLikes ? res[0].UserLikes.split(";").length : 0;

                    scope.post = res[0];
                });

                getAttachments.then(function (res) {
                    angular.forEach(res, function (attachment) {
                        attachment.FileName = attachment.LinkFilename.substr(0, attachment.LinkFilename.lastIndexOf('.'));
                        attachment.FileRef = attachment.FileRef.split(";#")[1];
                    });

                    scope.attachments = res;
                });

                scope.addComment = function () {
                    if (!scope.commentText) {
                        alert('Please, add comment text');
                        return false;
                    }
                    dataService.create({
                        listName: listNames.comments,
                        valuepairs: [
                            ['BodyText', scope.commentText],
                            ['PostID', scope.currentPostID]
                        ]
                    }).then(function () {
                        scope.commentText = '';
                        scope.$emit('commentAdded');

                        getPost().then(function (resObj) {
                            var commentsCounter = resObj[0]['Counter_x003a_Comments'] ? +resObj[0]['Counter_x003a_Comments'] : 0;

                            dataService.update({
                                listName: listNames.posts,
                                ID: scope.currentPostID,
                                valuepairs: [['Counter_x003a_Comments', commentsCounter + 1]]
                            });
                        });
                    });
                };

            } else {
                return false;
            }
        }
    }
}]);