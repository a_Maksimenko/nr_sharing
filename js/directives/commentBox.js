novoApp.directive('commentBox', ['dataService', 'listNames', 'helper', 'likesManager', 'globals', function (dataService, listNames, helper, likesManager, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'commentBox.html',
        scope: {},
        link: function (scope, elem, attr) {
            scope.currentPostID = (helper.getUrlVars()).postID; // Post ID
            scope.likeComment = function (ID) {
                var likedComment = null;
                angular.forEach(scope.comments, function (comment) {
                    if (comment.ID == ID) {
                        likedComment = comment;
                    }
                });
                likesManager.updateLikesNumber('comment', ID, likedComment);
            };

            var listQuery = {
                    "comments": "<Query><Where><Eq><FieldRef Name='PostID'></FieldRef><Value Type='Text'>" + scope.currentPostID + "</Value></Eq></Where><OrderBy><FieldRef Name='Created' Ascending='False' /></OrderBy></Query>"
                },
                getComments = function () {
                    return dataService.read({
                        listName: listNames.comments,
                        CAMLQuery: listQuery.comments
                    });
                },
                normalize = function (collection) {
                    angular.forEach(collection, function (comment) {
                        comment.Author = helper.parseName(comment.Author)["initials"].split(";#")[1];
                        comment.SubmissionDate = helper.getRelativeTime(comment.Created);
                        comment.LikesAmount = comment.UserLikes ? comment.UserLikes.split(";").length : 0;
                        comment.BodyText = (comment.BodyText).replace(/\n/g, '<br/>');
                    });
                    scope.comments = collection;
                };

            getComments().then(normalize);
            scope.$on('renderComments', function () {
                getComments().then(normalize);
            });
        }
    }
}]);