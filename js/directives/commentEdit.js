novoApp.directive('novoCommentEdit', ['dataService', 'listNames', 'getUser', 'globals', function (dataService, listNames, getUser, globals) {
    var commentList = listNames.comments;
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'commentEdit.html',
        scope: {},
        link: function (scope, elem, attr) {
            scope.comment = JSON.parse(attr.comment);
            scope.initialCommentText = angular.copy(scope.comment.BodyText);
            scope.isEditorActive = false;
            scope.$elem = elem;

            getUser.data().then(function (userData) {
                scope.currentUser = userData;
                scope.isAuthor = (scope.currentUser.initials == scope.comment.Author);
            });

            scope.editStart = function () {
                scope.isEditorActive = true;
                scope.$elem.closest('.j-comment').find('.j-comment-body').hide();
            };

            scope.cancelChanges = function () {
                scope.comment.BodyText = angular.copy(scope.initialCommentText);
                scope.isEditorActive = false;
                scope.$elem.closest('.j-comment').find('.j-comment-body').show();
            };

            scope.saveChanges = function () {
                var updateComment = dataService.update({
                    operation: 'UpdateListItems',
                    listName: commentList,
                    ID: scope.comment.ID,
                    valuepairs: [['BodyText', scope.comment.BodyText]]
                });

                updateComment.then(function () {
                    scope.initialCommentText = angular.copy(scope.comment.BodyText);
                    scope.isEditorActive = false;
                    scope.$elem.closest('.j-comment').find('.j-comment-body')
                        .text(scope.comment.BodyText)
                        .show();
                });
            };
        }
    }
}]);