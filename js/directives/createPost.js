novoApp.directive('createPost', ['getList', 'helper', 'dataService', 'listNames', 'globals', function (getList, helper, dataService, listNames, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'createPost.html',
        scope: {
            showPopup: '='
        },
        link: function (scope, elem, attr) {
            var resetData = function () {
                scope.activeCat = null;
                scope.activeCatID = null;
                scope.showCategories = false;
                scope.title = '';
                scope.bodyText = '';
                scope.currentStep = 'initial';
                scope.postID = null;
                scope.uploads = [];
            };

            resetData();
            getList.categories().then(function (res) {
                angular.forEach(res, function (item) {
                    item.Title = helper.setTM(item.Title);
                });
                scope.categories = res;
            });
            scope.setActive = function (category) {
                scope.activeCat = category.Title;
                scope.activeCatID = category.ID;
                scope.showCategories = false;
            };
            scope.closePopup = function () {
                if (scope.currentStep === 'uploads') {
                    var confirmed = window.confirm('Are you sure? All progress will be lost!');

                    if (confirmed) {
                        dataService.remove({
                            listName: listNames.posts,
                            ID: scope.postID
                        });
                        scope.showPopup = false;
                        resetData();
                    }
                } else {
                    scope.showPopup = false;
                    resetData();
                }
            };
            scope.addPost = function () {
                scope.title = helper.trim(scope.title);
                scope.bodyText = helper.trim(scope.bodyText);
                if (helper.isNotEmpty([scope.activeCatID, scope.title, scope.bodyText])) {
                    scope.currentStep = 'processing';
                    dataService.create({
                        listName: listNames.posts,
                        valuepairs: [
                            ['Title', scope.title],
                            ['BodyText', scope.bodyText],
                            ['CategoryID', scope.activeCatID],
                            ['Counter_x003a_Views', 0],
                            ['Counter_x003a_Likes', 0]
                        ]
                    }).then(function (resObj) {
                        scope.currentStep = 'uploads';
                        scope.postID = resObj[0].ID;
                    }, function () {
                        scope.currentStep = 'error';
                    });
                } else {
                    alert('Please, fill all data');
                }
                return false;
            };
            scope.finish = function () {
                if (!scope.uploads.length) {
                    alert('Please, attach at least one file!');
                    return false;
                }
                scope.currentStep = 'final';
                return false;
            };
            /*
            try of implementation. But client have not approved, so it was hidden !!!

            scope.createPostWithoutUpload = function () {
                scope.title = helper.trim(scope.title);
                scope.bodyText = helper.trim(scope.bodyText);
                if (helper.isNotEmpty([scope.activeCatID, scope.title, scope.bodyText])) {
                    scope.currentStep = 'processing';
                    dataService.create({
                        listName: listNames.posts,
                        valuepairs: [
                            ['Title', scope.title],
                            ['BodyText', scope.bodyText],
                            ['CategoryID', scope.activeCatID],
                            ['Counter_x003a_Views', 0],
                            ['Counter_x003a_Likes', 0]
                        ]
                    }).then(function (resObj) {
                        scope.currentStep = 'final';
                        return false;
                    }, function () {
                        scope.currentStep = 'error';
                    });
                } else {
                    alert('Please, fill all data');
                }
                return false;
            };
            */
        }
    }
}]);