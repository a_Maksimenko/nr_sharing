novoApp.directive('simpleFeed', ['dataService', 'listNames', 'helper', 'getList', '$q', 'globals', function (dataService, listNames, helper, getList, $q, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'simpleFeed.html',
        scope: {
            sortBy: '@'
        },
        link: function (scope, elem, attr) {
            var options = {
                    recent: '<Query><OrderBy><FieldRef Name="Created" Ascending="FALSE"/></OrderBy><Where></Where></Query>',
                    popular: '<Query><OrderBy><FieldRef Name="Counter_x003a_Likes" Ascending="FALSE"/></OrderBy><Where></Where></Query>'
                },
                getPosts = dataService.read({
                    listName: listNames.posts,
                    CAMLRowLimit: 6,
                    CAMLQuery: options[scope.sortBy]
                });

            $q.all([getList.categories(), getPosts]).then(function (res) {
                var categories = res[0],
                    posts = res[1],
                    catObj = {};

                angular.forEach(categories, function (category) {
                    catObj[category['ID']] = helper.setTM(category['Title']);
                });
                angular.forEach(posts, function (post) {
                    post['Author'] = helper.parseName(helper.removeID(post['Author'])).initials;
                    post['Category'] = {
                        ID: post['CategoryID'],
                        Title: catObj[post['CategoryID']]
                    };
                    post['Comments'] = post['Counter_x003a_Comments'] ? parseInt(post['Counter_x003a_Comments']) : 0;
                    post['Views'] = post['Counter_x003a_Views'] ? parseInt(post['Counter_x003a_Views']) : 0;
                    post['Likes'] = post['Counter_x003a_Likes'] ? parseInt(post['Counter_x003a_Likes']) : 0;
                });
                scope.items = posts;
            });
        }
    }
}]);