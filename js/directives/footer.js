novoApp.directive('novoFooter', ['globals', function (globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'footer.html'
    }
}]);