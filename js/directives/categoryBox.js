novoApp.directive('categoryBox', ['dataService', 'listNames', 'helper', 'getList', 'globals', function (dataService, listNames, helper, getList, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'categoryBox.html',
        scope: {
            showReset: '@'
        },
        link: function (scope, elem, attr) {
            scope.currentCatID = helper.getUrlVars().catID;
            scope.showResBtn = scope.currentCatID && scope.showReset;
            getList.categories().then(function (res) {
                var categoriesObj = {};

                angular.forEach(res, function (item) {
                    var parentTitle;

                    item['ParentCategory']['lookupValue'] = helper.setTM(item['ParentCategory']['lookupValue']);
                    parentTitle = item['ParentCategory']['lookupValue'];

                    if (!categoriesObj.hasOwnProperty(parentTitle)) {
                        categoriesObj[parentTitle] = [];
                    }

                    item.Title = helper.setTM(item.Title);
                    categoriesObj[item['ParentCategory']['lookupValue']].push(item);
                });
                scope.items = categoriesObj;
            });
        }
    }
}]);