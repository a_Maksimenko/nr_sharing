novoApp.directive('novoPostFeed', ['dataService', 'listNames', 'getList', 'helper', 'globals', function (dataService, listNames, getList, helper, globals) {
    return {
        restrict: 'A',
        templateUrl: globals.tmplUrl + 'postFeed.html',
        scope: {},
        link: function (scope, elem, attr) {
            var getItems = dataService.read({
                listName: listNames.posts,
                CAMLQuery: '<Query><OrderBy><FieldRef Name="Created" Ascending="FALSE"/></OrderBy></Query>'
            });

            scope.helper = helper;
            scope.filterCatId = helper.getUrlVars().catID;

            //order
            scope.order = {
                types: {
                    recent: '-Created',
                    popular: '-Counter_x003a_Likes' //'Counter_x003a_Comments'
                },
                setValue: function (orderType) {
                    this.value = this.types[orderType];
                },
                isActive: function (orderType) {
                    return (this.value == this.types[orderType]);
                }
            };
            scope.order.value = helper.getUrlVars().sortBy ? scope.order.types[helper.getUrlVars().sortBy] : scope.order.types.recent;

            //pagination
            scope.pagination = {
                maxPostAmount: 8,
                currentPage: 0,
                pageAmount: 0,
                pageList: [],
                goToPage: function (pageNumber) {
                    this.currentPage = pageNumber;
                    scope.postList = angular.copy(scope.initialPostList)
                        .splice(pageNumber * this.maxPostAmount, this.maxPostAmount);
                },
                goToPrevPage: function () {
                    if (this.currentPage != 0) {
                        this.goToPage(this.currentPage - 1);
                    }
                },
                goToNextPage: function () {
                    if (this.currentPage != this.pageList.length - 1) {
                        this.goToPage(this.currentPage + 1);
                    }
                }
            };

            //post list
            scope.initialPostList = [];

            getItems.then(function (res) {
                angular.forEach(res, function (item) {
                    if (!scope.filterCatId || scope.filterCatId == item['CategoryID']) {
                        item['Author'] = helper.parseName(item['Author'].split('#')[1]);
                        item['Counter_x003a_Comments'] = item['Counter_x003a_Comments'] ? parseInt(item['Counter_x003a_Comments']) : '';
                        item['Counter_x003a_Views'] = item['Counter_x003a_Views'] ? parseInt(item['Counter_x003a_Views']) : '';
                        item['Counter_x003a_Likes'] = item['Counter_x003a_Likes'] ? item['Counter_x003a_Likes'] : 0;
                        scope.initialPostList.push(item);
                    }
                });

                scope.pageAmount = Math.ceil(scope.initialPostList.length / scope.pagination.maxPostAmount);
                for (var i = 0; i < scope.pageAmount; i++) {
                    scope.pagination.pageList.push(i + 1);
                }
                scope.pagination.goToPage(0);

                //categories
                scope.categoryList = {};
                getList.categories().then(function (res) {
                    angular.forEach(res, function (item) {
                        scope.categoryList[item['ID']] = helper.setTM(item['Title']);
                    });
                });
            });
        }
    }
}]);