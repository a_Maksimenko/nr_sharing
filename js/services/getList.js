novoApp.service('getList', ['listNames', 'dataService', function (listNames, dataService) {
    var categoriesPromise = null;

    this.categories = function () {
        if (!categoriesPromise) {
            categoriesPromise = dataService.read({
                listName: listNames.categories,
                CAMLQuery: "<Query><OrderBy><FieldRef Name='Order' Ascending='True' /></OrderBy></Query>",
                mapping: {
                    ows_RelatedTo: {
                        mappedName: "ParentCategory",
                        objectType: "Lookup"
                    }
                }
            })
        }
        return categoriesPromise.then(function (res) {
            return angular.copy(res);
        });
    };
}]);