novoApp.service('postViewStatistic', ['$cookies', '$cookieStore', '$location', function ($cookies, $cookieStore, $location) {
    this.urlChanged = false;
    this.getCurrentPageUrl = function () {
        return $location.$$absUrl;
    };
    this.getPrevPageUrl = function () {
        return $cookieStore.get('pageUrl');
    };
    this.setPrevPageUrl = function () {
        $cookieStore.put('pageUrl', this.getCurrentPageUrl());
    };
    this.isUrlChanged = function () {
        this.urlChanged = this.getCurrentPageUrl() != this.getPrevPageUrl();
        return this.urlChanged;
    };
    this.checkUrlChange = function () {
        if (this.isUrlChanged()) {
            this.setPrevPageUrl();
        }
    };
}]);