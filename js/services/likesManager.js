novoApp.service('likesManager', ['listNames', 'dataService', 'getUser', function (listNames, dataService, getUser) {
    this.updateLikesNumber = function (type, ID, scope) {
        var listName = type == 'post' ? listNames.posts : listNames.comments;

        getUser.data().then(function (userData) {
            var currentUser = userData.initials;

            dataService.read({
                listName: listName,
                CAMLQuery: '<Query><Where><Eq><FieldRef Name="ID" /><Value Type="Text">' + ID + '</Value></Eq></Where></Query>'
            }).then(function (res) {
                var usersList = res[0].UserLikes ? res[0].UserLikes.split(';') : [];

                if (jQuery.inArray(currentUser, usersList) == -1) {
                    var updatedUsers = usersList.length == 0 ? currentUser : usersList.join(';') + ';' + currentUser;

                    dataService.update({
                        listName: listName,
                        valuepairs: [
                            ['UserLikes', updatedUsers],
                            ['Counter_x003a_Likes', usersList.length + 1]
                        ],
                        ID: ID
                    }).then(function () {
                        scope.LikesAmount = usersList.length + 1;
                    });
                } else {
                    alert('You\'ve already liked this ' + type);
                }
            });
        });
    }
}]);