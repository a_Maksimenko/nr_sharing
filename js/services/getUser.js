novoApp.service('getUser', ['$q', 'helper', function ($q, helper) {
    var userPromise = null;

    this.data = function () {
        if (!userPromise) {
            var getUserData = jQuery().SPServices.SPGetCurrentUser({
                fieldName: "Title",
                debug: false
            });

            userPromise = $q.when(getUserData).then(function (userString) {
                return helper.parseName(userString);
            });
        }
        return userPromise.then(function (res) {
            return angular.copy(res);
        });
    };
}]);