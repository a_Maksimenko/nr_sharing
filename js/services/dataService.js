novoApp.service('dataService', ['$q', function ($q) {
    var dataService = function (operation, batchCmd, options) {
        var delCmd = "<Batch OnError='Continue'><Method ID='1' Cmd='Delete'><Field Name='ID'>1</Field><Field Name='FileRef'>" + options.FileRef + "</Field></Method></Batch>",
            dataTransport = jQuery().SPServices({
                operation: operation,
                batchCmd: batchCmd,
                async: true,
                updates: options.FileRef ? delCmd : '',
                webURL: options.webURL || '',
                listName: options.listName,
                CAMLRowLimit: options.CAMLRowLimit || 0,
                CAMLQueryOptions: options.CAMLQueryOptions || '<QueryOptions></QueryOptions>',
                CAMLQuery: options.CAMLQuery || '',
                valuepairs: options.valuepairs || '',
                ID: options.ID || '',
                completefunc: function (xData, Status) {
                    return xData.responseXML;
                }
            });

        return $q.when(dataTransport).then(function (responseXML) {
            return jQuery(responseXML)
                .SPFilterNode("z:row")
                .SPXmlToJson({
                    mapping: options.mapping || {},
                    includeAllAttrs: true,
                    removeOws: true
                });
        });
    };

    this.create = function (options) {
        return dataService('UpdateListItems', 'New', options);
    };
    this.read = function (options) {
        return dataService('GetListItems', '', options);
    };
    this.update = function (options) {
        return dataService('UpdateListItems', 'Update', options);
    };
    this.remove = function (options) {
        return dataService('UpdateListItems', 'Delete', options);
    };
}]);