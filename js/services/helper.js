novoApp.service('helper', function () {
    var parseDate = function (input) {
        var parts = input.split(' ')[0].split('-');
        return new Date(parts[0], parts[1] - 1, parts[2]);
    };

    this.isNotEmpty = function (fields) {
        var result = true;

        angular.forEach(fields, function (field) {
            if (!field) result = false;
        });
        return result;
    };
    this.trunc = function (string, n) {
        return string.substr(0, n - 1) + (string.length > n ? '...' : '');
    };
    this.changeDate = function (date, getMonth) {
        var newDate = parseDate(date),
            month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][newDate.getMonth()];

        if (!getMonth) {
            return newDate.getDate() + ' ' + month;
        } else {
            return month;
        }
    };
    this.removeID = function (string) {
        if (string) {
            var result = string.split(';#')[1];

            if (result) {
                return result;
            } else {
                return string;
            }
        } else {
            return '';
        }
    };
    this.getUrlVars = function () {
        var href = window.location.href,
            hashes = href.slice(href.indexOf('?') + 1).split('&'),
            result = {};

        angular.forEach(hashes, function (item) {
            var keyVal = item.split('=');

            result[keyVal[0]] = keyVal[1];
        });
        return result;
    };
    this.declension = function (num, expressions) {
        var result,
            count = num % 100;

        if (count == 1) {
            result = expressions['0'];
        } else {
            result = expressions['1'];
        }
        return result;
    };
    this.getRelativeTime = function (date) {
        return moment(date).fromNow();
    };
    this.parseName = function (nameString) {
        var regExp = /\(([^)]+)\)/,
            userFullName = (regExp.exec(nameString))[1];

        return {
            initials: nameString.split(' ')[0],
            firstName: userFullName.split(' ')[0],
            lastName: userFullName.split(' ')[1],
            fullName: userFullName
        }
    };
    this.setTM = function(string) {
        return string.replace(/&reg;/g, '<sup>&reg;</sup>');
    };
    this.trim = function(string) {
        return string.replace(/^\s+|\s+$/g, '');
    }
});