var novoApp = angular.module('novoApp', ['ngCookies', 'ngSanitize']);

novoApp.constant('listNames', {
    categories: 'Discussion: Categories',
    attachments: 'Discussion: Attachments',
    comments: 'Discussion: Comments',
    posts: 'Discussion: Posts'
});

novoApp.constant('globals', {
    tmplUrl: 'System/html/tmpl/'
});

novoApp.run(['postViewStatistic', function(postViewStatistic) {
    postViewStatistic.checkUrlChange();
}]);