novoApp.controller('pageCtrl', ['$scope', function ($scope) {
    $scope.$on('commentAdded', function () {
        $scope.$broadcast('renderComments');
    });
}]);