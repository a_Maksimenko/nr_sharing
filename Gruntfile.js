module.exports = function (grunt) {
    grunt.initConfig({
        options: {
            beautify: {
                ascii_only: true
            }
        },
        uglify: {
            'js/main.min.js': [
                'js/lib/jquery.SPServices-2014.02.min.js',
                'js/lib/jquery.SPWidgets.min.js',
                'js/lib/jquery.xml2json.min.js',
                'js/lib/angular.min.js',
                'js/lib/angular-cookies.min.js',
                'js/lib/angular-sanitize.min.js',
                'js/lib/moment.min.js',
                'js/app.js',
                'js/templates.js',
                'js/services/*.js',
                'js/controllers/*.js',
                'js/directives/*.js'
            ]
        },
        cssmin: {
            minify: {
                src: [
                    'css/_reset.css',
                    'css/_layout.css',
                    'css/_typography.css',
                    'css/_footer.css',
                    'css/_header.css',
                    'css/_boxes.css',
                    'css/_popup.css',
                    'css/_post.css',
                    'css/_postFeed.css'
                ],
                dest: 'css/main.min.css'
            }
        },
        ngtemplates: {
            novoApp: {
                src: 'html/tmpl/**.html',
                dest: 'js/templates.js',
                options: {
                    prefix: 'System/',
                    htmlmin: {
                        collapseWhitespace: true,
                        collapseBooleanAttributes: true
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-angular-templates');

    grunt.registerTask('js-build', ['uglify']);
    grunt.registerTask('css-build', ['cssmin']);
    grunt.registerTask('template-build', ['ngtemplates']);

    grunt.registerTask('default', ['template-build', 'js-build', 'css-build']);
};